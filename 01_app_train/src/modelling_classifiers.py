import pandas as pd
import numpy as np
import datetime
import time
import operator
from collections import OrderedDict
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
import os
import glob

# PreProcessing Loading
from sklearn.model_selection import cross_val_predict
from sklearn import model_selection
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler, StandardScaler, LabelEncoder
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

# ALGORITHMS LOADING
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from xgboost import XGBClassifier, XGBRegressor
import lightgbm as lgb
#import lightgbm as lgb

# ML Packages
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier, NearestCentroid, RadiusNeighborsClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, BaggingClassifier, GradientBoostingClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis, LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression, PassiveAggressiveClassifier, Perceptron, RidgeClassifier, RidgeClassifierCV, SGDClassifier
from sklearn.calibration import CalibratedClassifierCV
from sklearn.semi_supervised import LabelPropagation, LabelSpreading

from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, classification_report

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, confusion_matrix





#import logging
#logging.basicConfig(filename='classifiers.log', filemode='w', format='%(asctime)s %(levelname)-8s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')




import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report

"""
url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/pima-indians-diabetes.data.csv"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = pandas.read_csv(url, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]
test_size = 0.33
seed = 7
X_train, X_test, y_train, y_test = model_selection.train_test_split(X, Y, test_size=test_size, random_state=seed)
"""







def feature_importance(X_train, y_train):
    #FEATURE IMPORTANCE GRADIENT BOOSTING AND RANDOM FOREST
    clf_gb = GradientBoostingClassifier(random_state = 42)
    clf_gb.fit(X_train, y_train)
    score = round((clf_gb.score(X_train, y_train)) * 100, 2)
    # GradientBoostingRegressor feature importance - top 10
    feat_importances_01 = pd.Series(clf_gb.feature_importances_, index=X_train.columns)
    values = list(feat_importances_01)
    keys = list(X_train.columns)
    dictionary = dict(zip(keys, values))
    d_descending = OrderedDict(sorted(dictionary.items(), key=lambda kv: kv[1], reverse=True))
    #feat_importance_gb = list(feat_importances_01.nlargest(5))
    #sorted_x = sorted(dictionary.items(), key=operator.itemgetter(1))
    clf_rf = RandomForestClassifier(random_state = 42)
    clf_rf.fit(X_train, y_train)
    score2 = round((clf_rf.score(X_train, y_train)) * 100, 2)
    # GradientBoostingRegressor feature importance - top 10
    feat_importances_02 = pd.Series(clf_rf.feature_importances_, index=X_train.columns)
    values2 = list(feat_importances_02)
    keys2 = list(X_train.columns)
    dictionary2 = dict(zip(keys2, values2))
    d_descending2 = OrderedDict(sorted(dictionary2.items(), key=lambda kv: kv[1], reverse=True))
    logging.info('feature importance done!')
    #return {'score_gbm' : score, 'feature_importance_gbm': d_descending,
    #'score_rf' : score2, 'feature_importance_rf': d_descending2}
    return {'score_rf' : score2, 'feature_importance_rf': d_descending2}

#print(feature_importance)


#CLASSIFIER WITH CROSS VALIDATION
models = []
models.append(('Logistic Regression', LogisticRegression()))
models.append(('K-NearestNeighbors', KNeighborsClassifier()))
models.append(('Decision Tree', DecisionTreeClassifier()))
models.append(('Random Forest', RandomForestClassifier()))
models.append(('Support Vector Machines', SVC()))
# evaluate each model in turn

modelling = {'Logistic Regression' : LogisticRegression(),
'K-NearestNeighbors' : KNeighborsClassifier(),
'Decision Tree' : DecisionTreeClassifier(),
'Random Forest' : RandomForestClassifier(),
'Support Vector Machines' : SVC()}

def cvclassifier(X, y, models, modelling):
    results = []
    names = []
    allmodels = []
    scoring = 'accuracy'
    for name, model in models:
        kfold = model_selection.KFold(n_splits=10)
        cv_results = model_selection.cross_val_score(model, X, y, cv=kfold, scoring=scoring)
        results.append(cv_results.mean())
        names.append(name)
        score1 = cv_results.mean()
        msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        allmodels.append(msg)
        model_results = results
        model_names = names
        resultados1 = dict(zip(model_names, model_results))
        resultados2 = OrderedDict(sorted(resultados1.items(), key=lambda kv: kv[1], reverse=True))
        best = list(resultados2.keys())[0]
        best2 = list(resultados2.values())[0]


        #for i in best:
        #	if i in modelling:
        clf = modelling[best]
        y_pred = cross_val_predict(clf, X, y, cv=10)
        prediction = y_pred.tolist()
    return {'cv_resultados' : resultados2, 'best' : best, 'best_score' : best2, 'forecast_cv' : prediction}

#print(classifier_cv)






def stdclassifiers(X_train, y_train, X_test, y_test):
    names = list(modelling.keys())
    classifiers = list(modelling.values())
    lista = []
    for name, clf in zip(names, classifiers):
        try:
            #t_start = time()
            fit = clf.fit(X_train, y_train)
            #t_end = time()
            #t_diff = t_end - t_start
            score_test = fit.score(X_test, y_test)
            score_train = fit.score(X_train, y_train)
            predicted = fit.predict(X_test)
            f1 = f1_score(y_test, predicted)
            precision = precision_score(y_test, predicted)
            recall = recall_score(y_test, predicted)
            results1 = (name, score_test, f1, precision, recall)
            lista.append(results1)
            logging.info('{} trained!'.format(name))
        except Exception:
            pass
    results = sorted(lista, key=lambda x: x[1], reverse=True)
    return {'std_classifiers' : results}

def stdclassifiers1(df, X_train, y_train, X_test, y_test, X):
    names = list(modelling.keys())
    classifiers = list(modelling.values())
    lista = []
    for name, clf in zip(names, classifiers):
            t_start = time.time()
            fit = clf.fit(X_train, y_train)
            t_end = time.time()
            t_diff = t_end - t_start
            score_test = fit.score(X_test, y_test)
            score_train = fit.score(X_train, y_train)
            predicted = fit.predict(X_test)
            f1 = f1_score(y_test, predicted)
            precision = precision_score(y_test, predicted)
            recall = recall_score(y_test, predicted)
            results1 = (name, score_test, f1, precision, recall, t_diff)
            lista.append(results1)
    results = sorted(lista, key=lambda x: x[1], reverse=True)

    best = results[0][0]
    clf = modelling[best]
    clf.fit(X_train, y_train)
    predict = clf.predict(X)

    proba_yes = clf.predict_proba(X)[:,1]
    proba_no = 1 - proba_yes
    proba_yes1 = pd.DataFrame({'proba_yes': proba_yes})
    proba_no1 = pd.DataFrame({'proba_no': proba_no})
    predict1 = pd.DataFrame({'predict': predict})

    df_results = df.join(proba_yes1)
    df_results2 = df_results.join(proba_no1)
    df_results3 = df_results2.join(predict1)

    results2 = {'std_classifiers' : results, 'df_results': df_results3}

    return results2



def gbm(X_train, y_train, X_test, y_test):
    #LIGHT GRADIENT BOOSTING MODELLING
    d_train = lgb.Dataset(X_train, label=y_train)
    params = {}
    params['learning_rate'] = 0.003
    params['boosting_type'] = 'gbdt'
    params['objective'] = 'binary'
    params['metric'] = 'binary_logloss'
    params['sub_feature'] = 0.5
    params['num_leaves'] = 10
    params['min_data'] = 50
    params['max_depth'] = 10
    clf = lgb.train(params, d_train, 100)
    threshold = 0.4
    y_pred=clf.predict(X_test)
    y_pred = (y_pred>threshold).astype(int)
    y_pred = y_pred.tolist()
    accuracy=accuracy_score(y_pred,y_test)
    return {'lgbm_accuracy' : accuracy, 'prediction' : y_pred}


def xgboost1(X_train, y_train, X_test, y_test):
    #XGBOOST
    X_train = X_train.as_matrix()
    X_test1 = X_test.as_matrix()
    xgb = XGBClassifier()
    xgb.fit(X_train, y_train)

    # make predictions for test data
    y_pred = xgb.predict(X_test1)
    xgb_predictions = [round(value) for value in y_pred]
    xgb_accuracy = accuracy_score(y_test, xgb_predictions)
    xgb_accuracy2 = str(xgb_accuracy)

    return {'xgb_score': xgb_accuracy, 'xgb_predictions' : xgb_predictions}

"""
if __name__ == '__main__':
    print(stdclassifiers(names, classifiers, X_train, y_train, X_test, y_test))
"""
