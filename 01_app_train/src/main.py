import os
import errno
import shutil

items = ["01", "02", "03", "04", "05"]
models = ["print(feature_importance(X_train, y_train))", "print(cvclassifier(X, y, models, modelling))", "print(stdclassifiers1(X_train, y_train, X_test, y_test))", "print(gbm(X_train, y_train, X_test, y_test))", "print(xgboost1(X_train, y_train, X_test, y_test))"]

for item, model in zip(items, models):
    dir_path = "app_{}".format(item)
    # check if directory exists or not yet
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    
    file_path = "app_{}.py".format(item)
    with open(file_path, "w") as f:
        f.write("import pandas as pd")
        f.write("\nfrom sklearn.model_selection import train_test_split")
        f.write("\nfrom modelling_classifiers import feature_importance, cvclassifier, stdclassifiers1, gbm, xgboost1")
        f.write("\nurl = 'https://raw.githubusercontent.com/jbrownlee/Datasets/master/pima-indians-diabetes.data.csv'")
        f.write("\nnames = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']")
        f.write("\ndataframe = pd.read_csv(url, names=names)")
        f.write("\narray = dataframe.values")
        f.write("\nX = array[:,0:8]")
        f.write("\nY = array[:,8]")
        f.write("\ntest_size = 0.33")
        f.write("\nseed = 7")
        f.write("\nX_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=test_size, random_state=seed)\n")
        f.write(model)
        
        # move files into created directory
        shutil.move(file_path, dir_path)
    
    file_path2 = "Dockerfile"

    with open(file_path2, "w") as d:
        d.write('FROM frolvlad/alpine-python-machinelearning as ' + dir_path)
        d.write("\nWORKDIR /src")
        d.write("\nRUN ls -la /src/*")
        d.write("\nCOPY " + "/src/" + dir_path + "/" + file_path + " /src")
        d.write('\nCMD ' + '["python3", ' + "'" + '/src/' + file_path + "']")

        shutil.move(file_path2, dir_path)



